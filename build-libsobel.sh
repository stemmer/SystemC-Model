#!/usr/bin/env bash


#source ../scripts/settings.sh
SOURCEPATH="../PlatformV2/lib/src/sobel"
INSTALLPATH="./apps/"
BUILDPATH="./build-sobel"
INCLUDEPATHS="-I$SOURCEPATH -I./"
SHARED_CFLAGS="-O0 -c"
GCC=gcc
AR=ar


# 1st argument: libname "jpeg-ef" for example
# 2nd argument: CFLAGS
function Build
{
    LIBNAME="$1"
    CFLAGS="$2"

    SOURCES=$(find $SOURCEPATH -type f -name "*.c")
    for s in $SOURCES ; 
    do
        echo -e -n "\e[1;34m - $s\e[0m "
        FILENAME="${s##*/}"
        OBJFILE="${FILENAME%.*}.o"
        $GCC $CFLAGS $INCLUDEPATHS -I$SOURCEPATH -c -o "$BUILDPATH/$OBJFILE" $s
        if [ $? -ne 0 ] ; then
            echo -e "\e[1;31m$GCC FAILED!\e[0m"
            exit 1
        else
            echo -e "\e[1;32m✔\e[0m"
        fi
    done

    $AR rv $INSTALLPATH/lib${LIBNAME}.a $BUILDPATH/*.o
    rm $BUILDPATH/*.o
}

# Build Libraries
mkdir -p $BUILDPATH
rm $INSTALLPATH/libsobel.a
Build sobel $SHARED_CFLAGS
cp $SOURCEPATH/*.h $INSTALLPATH/.

# Make Clean
rmdir $BUILDPATH

