#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP

#include <sdf.h>

void MB0::Execute()
{
    token_t tokens[64] = {0x00};

    // Init ch_dcoffset channel with 3 tokens!
    this->WriteTokens(ch_dcoffset, tokens);

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        ITERATION_BEGIN();


        this->ReadTokens(ch_dcoffset, tokens);
        WAIT_COMPUTE(GetEncodedImageBlock);
        this->WriteTokens(ch_ency,  tokens);
        this->WriteTokens(ch_enccr, tokens);
        this->WriteTokens(ch_enccb, tokens);
        this->WriteTokens(ch_dcoffset, tokens);


        this->ReadTokens(ch_ency, tokens);
        WAIT_COMPUTE(IQ_Y);
        this->WriteTokens(ch_prepy, tokens);


        this->ReadTokens(ch_enccr, tokens);
        WAIT_COMPUTE(IQ_Cr);
        this->WriteTokens(ch_prepcr, tokens);


        this->ReadTokens(ch_enccb, tokens);
        WAIT_COMPUTE(IQ_Cb);
        this->WriteTokens(ch_prepcb, tokens);


        this->ReadTokens(ch_prepy, tokens);
        WAIT_COMPUTE(IDCT_Y);
        this->WriteTokens(ch_y, tokens);


        this->ReadTokens(ch_prepcr, tokens);
        WAIT_COMPUTE(IDCT_Cr);
        this->WriteTokens(ch_cr, tokens);


        this->ReadTokens(ch_prepcb, tokens);
        WAIT_COMPUTE(IDCT_Cb);
        this->WriteTokens(ch_cb, tokens);


        this->ReadTokens(ch_y,  tokens);
        this->ReadTokens(ch_cr, tokens);
        this->ReadTokens(ch_cb, tokens);
        WAIT_COMPUTE(CreateRGBPixels);


        ITERATION_END();
    }
}

void MB1::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB2::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB3::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB4::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB5::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB6::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}



#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

