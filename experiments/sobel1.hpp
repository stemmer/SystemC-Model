#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP

#include <sdf.h>

void MB0::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB1::Execute()
{
    token_t tokens[81] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        ITERATION_BEGIN();


        WAIT_COMPUTE(GetPixel_EA);
        this->WriteTokens(ch_gx, tokens);
        this->WriteTokens(ch_gy, tokens);


        this->ReadTokens(ch_gy, tokens);
        WAIT_COMPUTE(GY_EA);
        this->WriteTokens(ch_ya, tokens);


        this->ReadTokens(ch_gx, tokens);
        WAIT_COMPUTE(GX_EA);
        this->WriteTokens(ch_xa, tokens);


        this->ReadTokens(ch_xa, tokens);
        this->ReadTokens(ch_ya, tokens);
        WAIT_COMPUTE(ABS);


        ITERATION_END();
    }
}

void MB2::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB3::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB4::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB5::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB6::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}



#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

