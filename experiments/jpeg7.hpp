#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP

#include <sdf.h>

void MB0::Execute()
{
    token_t tokens[64] = {0x00};

    // Init ch_dcoffset channel with 3 tokens!
    this->WriteTokens(ch_dcoffset, tokens);

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        ITERATION_BEGIN();


        this->ReadTokens(ch_dcoffset, tokens);
        WAIT_COMPUTE(GetEncodedImageBlock);
        this->WriteTokens(ch_ency,  tokens);
        this->WriteTokens(ch_enccr, tokens);
        this->WriteTokens(ch_enccb, tokens);
        this->WriteTokens(ch_dcoffset, tokens);


        this->ReadTokens(ch_y,  tokens);
        this->ReadTokens(ch_cr, tokens);
        this->ReadTokens(ch_cb, tokens);
        WAIT_COMPUTE(CreateRGBPixels);


        ITERATION_END();
    }
}

void MB1::Execute()
{
    token_t tokens[64] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_ency, tokens);
        WAIT_COMPUTE(IQ_Y_EA);
        this->WriteTokens(ch_prepy, tokens);
    }
}

void MB2::Execute()
{
    token_t tokens[64] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_enccr, tokens);
        WAIT_COMPUTE(IQ_Cr_EA);
        this->WriteTokens(ch_prepcr, tokens);
    }
}

void MB3::Execute()
{
    token_t tokens[64] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_enccb, tokens);
        WAIT_COMPUTE(IQ_Cb_EA);
        this->WriteTokens(ch_prepcb, tokens);
    }
}

void MB4::Execute()
{
    token_t tokens[64] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_prepy, tokens);
        WAIT_COMPUTE(IDCT_Y_EF);
        this->WriteTokens(ch_y, tokens);
    }
}

void MB5::Execute()
{
    token_t tokens[64] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_prepcr, tokens);
        WAIT_COMPUTE(IDCT_Cr_EF);
        this->WriteTokens(ch_cr, tokens);
    }
}

void MB6::Execute()
{
    token_t tokens[64] = {0x00};

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_prepcb, tokens);
        WAIT_COMPUTE(IDCT_Cb_EF);
        this->WriteTokens(ch_cb, tokens);
    }
}



#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

