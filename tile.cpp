#include <tile.hpp>
#include <iostream>

Tile::Tile(sc_core::sc_module_name name)
    : core::Master(sc_core::sc_module_name(name))
{
}



void Tile::WriteTokens(Channel &channel, token_t tokens[])
{
    // Initialization Block
    unsigned int usage;
    sc_core::wait(1, sc_core::SC_NS);

    // Polling Block
    do
    {
        this->ReadWord(channel.usageaddress, &usage);
        sc_core::wait(1, sc_core::SC_NS);
        if(usage == 0)
            break;
        sc_core::wait(2, sc_core::SC_NS);

    }
    while(true);

    // Preparation Block
    unsigned int index;
    index = 0;
    sc_core::wait(4, sc_core::SC_NS);

    // Copy Block
    for(unsigned int tokenindex = 0; tokenindex < channel.producerate; tokenindex++)
    {
        unsigned long long address;
        address  = channel.fifoaddress;
        address += index;
        sc_core::wait(2, sc_core::SC_NS);
        this->WriteWord(address, reinterpret_cast< unsigned int* >(&tokens[tokenindex]));
        index   += 1;
        sc_core::wait(5, sc_core::SC_NS);
    }
    
    // Management Block
    usage = 1;
    this->WriteWord(channel.usageaddress, &usage);
    sc_core::wait(3, sc_core::SC_NS);
}



void Tile::ReadTokens(Channel &channel, token_t tokens[])
{
    // Initialization Block
    unsigned int usage;
    sc_core::wait(1, sc_core::SC_NS);

    // Polling Block
    do
    {
        this->ReadWord(channel.usageaddress, &usage);

        sc_core::wait(1, sc_core::SC_NS);
        if(usage != 0)
            break;
        sc_core::wait(2, sc_core::SC_NS);
    }
    while(true);

    // Preparation Block
    unsigned int index;
    index = 0;
    sc_core::wait(5, sc_core::SC_NS);

    // Copy Block
    for(unsigned int tokenindex = 0; tokenindex < channel.consumerate; tokenindex++)
    {
        unsigned long long address;
        address  = channel.fifoaddress;
        address += index;
        this->ReadWord(address, reinterpret_cast< unsigned int* >(&tokens[tokenindex]));
        index   += 1;

        sc_core::wait(5, sc_core::SC_NS);
        if(tokenindex+1 < channel.consumerate)
            sc_core::wait(2, sc_core::SC_NS);
    }
    sc_core::wait(1, sc_core::SC_NS);

    // Update meta data
    usage = 0;
    this->WriteWord(channel.usageaddress, &usage);
    sc_core::wait(3, sc_core::SC_NS);
}


// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

