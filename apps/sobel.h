/*
 * sobel_filter.h
 *
 *  Created on: 10.12.2015
 *      Author: Christof Schlaak
 *      Refactored: Ralf Stemmer on 16.05.2018
 */

#ifndef SOBEL_FILTER_H_
#define SOBEL_FILTER_H_

#include <sdf.h>

void GetPixel(token_t tokensOut[]);
void GX(token_t tokensIn[], token_t tokensOut[]);
void GY(token_t tokensIn[], token_t tokensOut[]);
token_t ABS(token_t tokensIn1[], token_t tokensIn2[]);
#endif /* SOBEL_FILTER_H_ */
