#ifndef TILE_HPP
#define TILE_HPP

#include <core/master.hpp>
#include <channel.hpp>

#define xstr(a) str(a)
#define str(a) #a

// This method creates a new class that is derived from the Tile class
#define CreateTile(s) class s : public Tile \
                        { \
                        public: \
                            s() : Tile(sc_core::sc_module_name(xstr(s))){}; \
                        protected: \
                            virtual void Execute(); \
                        };

class Tile : public core::Master
{
    public:
        Tile(sc_core::sc_module_name name);
        ~Tile(){};

        virtual void Execute() = 0;
        void WriteTokens(Channel &channel, token_t tokens[]);
        void ReadTokens(Channel &channel, token_t tokens[]);

    private:
};

#endif
// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

