#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include <tile.hpp>

CreateTile(MB0);
CreateTile(MB1);
CreateTile(MB2);
CreateTile(MB3);
CreateTile(MB4);
CreateTile(MB5);
CreateTile(MB6);

#if 0
class MB0 : public Tile
{
    public:
        MB0() : Tile(sc_core::sc_module_name("MB0")) {};
    protected:
        virtual void Execute();
};

class MB1 : public Tile
{
    public:
        MB1() : Tile(sc_core::sc_module_name("MB1")) {};
    protected:
        virtual void Execute();
};

class MB2 : public Tile
{
    public:
        MB2() : Tile(sc_core::sc_module_name("MB2")) {};
    protected:
        virtual void Execute();
};

class MB3 : public Tile
{
    public:
        MB3() : Tile(sc_core::sc_module_name("MB3")) {};
    protected:
        virtual void Execute();
};

class MB4 : public Tile
{
    public:
        MB4() : Tile(sc_core::sc_module_name("MB4")) {};
    protected:
        virtual void Execute();
};

class MB5 : public Tile
{
    public:
        MB5() : Tile(sc_core::sc_module_name("MB5")) {};
    protected:
        virtual void Execute();
};

class MB6 : public Tile
{
    public:
        MB6() : Tile(sc_core::sc_module_name("MB6")) {};
    protected:
        virtual void Execute();
};
#endif


#endif
// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

