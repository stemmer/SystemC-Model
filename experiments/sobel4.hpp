#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP

#include <sdf.h>

extern "C"
{
#include <apps/sobel.h>
}

void MB0::Execute()
{
    token_t xtokens[1];
    token_t ytokens[1];
    token_t result;

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_xa, xtokens);
        this->ReadTokens(ch_ya, ytokens);
        result = ABS(xtokens, ytokens);
        //std::cerr << " " << std::dec << result;
        //if((i+1)%48 == 0) std::cerr << "\n";
        WAIT_COMPUTE(ABS);

        ITERATION_END();
    }
}

void MB1::Execute()
{
    token_t tokens[81];

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        ITERATION_BEGIN();

        GetPixel(tokens);
        WAIT_COMPUTE(GetPixel_EA);
        this->WriteTokens(ch_gx, tokens);
        this->WriteTokens(ch_gy, tokens);
    }
}

void MB2::Execute()
{
    token_t tokens_in[81];
    token_t tokens_out[1];

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_gx, tokens_in);
        GX(tokens_in, tokens_out);
        WAIT_COMPUTE(GX_EA);
        this->WriteTokens(ch_xa, tokens_out);
    }
}

void MB3::Execute()
{
    token_t tokens_in[81];
    token_t tokens_out[1];

    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        this->ReadTokens(ch_gy, tokens_in);
        GY(tokens_in, tokens_out);
        WAIT_COMPUTE(GY_EA);
        this->WriteTokens(ch_ya, tokens_out);
    }
}

void MB4::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB5::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}

void MB6::Execute()
{
    for(int i=SKIP_ITERATIONS; i<NUM_OF_ITERATIONS; i++)
    {
        sc_core::wait(sc_core::SC_ZERO_TIME);
    }
}



#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

